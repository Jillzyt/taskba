```
Student Name: Zeng Yu Ting
Matriculation Number: A0202284N

Link to github repository: https://gitlab.com/Jillzyt/taskba
```

# Run the API locally, including Postman calls used to demonstrate a working API. Be sure to include the edge cases tested or proof of error-resiliency as per the marking scheme. (B1)
## Steps to run this project locally

1. Firstly, install these components (The instructions will vary depending on the OS)
- php version 8 (https://www.php.net/releases/8.0/en.php)
- composer (https://getcomposer.org/download/)
- npm package (https://phoenixnap.com/kb/install-node-js-npm-on-windows) (https://treehouse.github.io/installation-guides/mac/node-mac.html)

2. Set up a local database <br>
Create a local mysql database and note down the following values which will be relevant

```
DB_CONNECTION=mysql
DB_HOST=xxxxx
DB_PORT=3306
DB_DATABASE=xxxxx
DB_USERNAME=xxxxx
DB_PASSWORD=xxxx
```

3. Fill these values in the .env.example file with what you just changed

These are my values as an example
```
DB_CONNECTION=mysql
DB_HOST=192.168.64.4 # You should fill up your own values
DB_PORT=3306 # You should fill up your own values
DB_DATABASE=task_b # You should fill up your own values
DB_USERNAME=username # You should fill up your own values
DB_PASSWORD=password # You should fill up your own values
```

4. Make a copy of ```.env.example``` file and rename it to ```.env```

5. Run these commands

```
composer install
npm install
php artisan migrate
```

6. Run the command to start hosting it on the localhost.
```
php artisan serve
```

It should display this result <br>
<img src="./images/localhost.png" width="1200" height="125">
<br>
<br>
You can then access to the page to check if it's working at http://127.0.0.1:8000 (This url varies depending on the system)


## Test out the API on Local (B1)

### Local
The postman routes are stored under Payments Local folder in Postman.

Public link to a static snapshot of the collection:
https://www.getpostman.com/collections/ce41d7236c59dd99e063

Link to documentation:
https://documenter.getpostman.com/view/13126641/U16kpjH1

You can change the {{base_url}} to your localhost url or add an environment variable base_url in post.

<b>For delete and update payment, do change id to the ID of an existing payment or there might be an error. </b>

# Access the deployed API (B1/B3)

## Local
Mentioned above. 

## Deployed environment: https://crimson-meadow-iflpqzhxg6gi.vapor-farm-b1.com
The postman routes are stored under Payments Public Environment folder in Postman.

Public link to a static snapshot of the collection:
https://www.getpostman.com/collections/f551ffc9a8d0db76c046

Link to documentation:
https://documenter.getpostman.com/view/13126641/U16kpjCj

<b>For delete and update payment, do change id to the ID of an existing payment or there might be an error which is NOT FOUND. </b> <br/>
<b>All the fields can be nullable, hence empty payments can be added. However, when adding payment_amount field, it needs to be lesser or equal to 8 characters (including decimal place). </b>

# Run tests locally and via Travis (B2)
## Local
To run the test locally, <br>
As note that we have .env.testing file <br><br>
Base on the documentation, the file .env.testing will be used instead of the .env file when running PHPUnit tests or executing Artisan commands with the --env=testing option. <br>
<b> Hence, set up a database for testing or modify .env file according to your database value.</b> For reference also, the test cases is located under the tests folder. <br/>

<b> To run the test locally, the command is : ./vendor/bin/phpunit </b><br><br>

The result should look like this: <br>
<img src="./images/localtests.png" width="900" height="250">
<br>

If there's an error, you may have to recheck your database configurations. <br>

## GitLab
Instead of using Travis, I had decided to use GitLab. After much attempts, I managed to set up the gitlab CI tool. 

How did I do it? <br>
I created a repository under gitlab named taskba. GitLab have their own CI/CD tools which was helpful.

With the .gitlab.ci.yml (which is also in this repo), I planned to have two stages of testing and the last stage being deployment.The scripts helps me to build the app and then test it with the same command as ./vendor/bin/phpunit in the docker image and then deploy the application. <br/>
<img src="./images/gitlab.png" width="1000" height="500">
<br>
<br>

The results of the test jobs look like this which is similar to the one on our local. For reference, the test cases is located under the tests folder.
<img src="./images/jobsuccessful.png" width="2000" height="800">
<br> 

After the testing is completed, I will deploy the application to Laravel Vapor which is shown in the last stage of my .gitlab-ci.yml .

### How was testing triggered?
Everytime I push something into the repository under master branch, the repo will go through CI tests and you can check the results under the CI pipeline.

### Deployment using CI/CD pipeline
I have used Laravel Vapor which is a serverless deployment platform for Laravel, powered by AWS.(This is a paid service.) In Gitlab, I added the last stage of CI/CD pipeline which is the deployment stage. <br/>

```
deploy:
  stage: staging_deploy
  script:
    - apt-get --assume-yes install npm
    - composer global require laravel/vapor-cli
    - composer require laravel/vapor-core --update-with-dependencies
    - php vendor/bin/vapor deploy staging
```
Firstly, the docker image will download the npm package manager so that I can run npm package later on with my vapor.yml.  <br/>
I added the requirements required by composer to initiate vapor.  <br/>
I also added VAPOR_API_TOKEN environment variable as required. (More info at https://docs.vapor.build/1.0/projects/deployments.html under CI platform section)  <br/>
After this, I run the vapor.yml script with the command ```php vendor/bin/vapor deploy staging``` <br/>

This is shown after the CD pipeline in GitLab has been run successfully: <br>
<img src="./images/vaporsuccessful.png" width="1000" height="500">
<br>

# Set up frontend (B4)
## Steps to run this project locally

The steps are similar to B1's set up. 

After you have set up the project, </br>

Run the command to start hosting it on the localhost.
```
php artisan serve
```

It should display this result <br>
<img src="./images/localhost.png" width="1200" height="125">
<br>
<br>
You can then access to the page to check if it's working at http://127.0.0.1:8000 (This url varies depending on the system) <br> 

This is the first page.<br/>
<img src="./images/frontend.png" width="1000" height="500">
<br/>

You will be required to register for an account before logging in.<br/>
After you login, you can access the page payments to play with the functions <br/>
<img src="./images/paymentsfrontend.png" width="1000" height="450">
<br/>


## Deployment

You can also access to https://crimson-meadow-iflpqzhxg6gi.vapor-farm-b1.com to access to the frontend.
Likewise, 

This is the first page.<br/>
<img src="./images/frontend.png" width="1000" height="500">
<br/>

You will be required to register for an account before logging in.<br/>
After you login, you can access the page payments to play with the functions <br/>
<img src="./images/paymentsfrontend.png" width="1000" height="450">
<br/>

# Additional notes

The use case of the application is CRUD payment objects :).

I tried inviting the two usernames for marking into gitlab but unable to add.
I am also willing to do an demo for my grades if circumstances come to so.