@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('View Payment') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>Freelancer Reference: {{$payment->freelancer_ref}}</p>
                    <p>Payer Name: {{$payment->payer_name}}</p>
                    <p>Payer Email: {{$payment->payer_email}}</p>
                    <p>Invoice Reference: {{$payment->invoice_ref}}</p>
                    <p>Payment Type: {{$payment->payment_type}}</p>
                    <p>Currency: {{$payment->currency}}</p>
                    <p>Payment Amount: {{$payment->payment_amount}}</p>
                    <p>Payment Status: {{$payment->payment_status}}</p>
                    <p>Payment Created At: {{date('Y-m-d', strtotime($payment->created_at))}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection