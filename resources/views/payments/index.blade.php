@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
    <div class="col-12">
                @if ($message = Session::get('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>    
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                <a href="payments/create" class="btn btn-primary mb-2">Create Payment</a> 
                <br>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Freelancer Reference</th>
                            <th>Payer Name</th>
                            <th>Payer Email</th>
                            <th>Invoice Reference</th>
                            <th>Payment Type</th>
                            <th>Currency</th>
                            <th>Payment Amount</th>
                            <th>Payment Status</th>
                            <th colspan="2">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($payments as $payment)
                        <tr>
                            <td>{{ $payment->id }}</td>
                            <td>{{ $payment->freelancer_ref }}</td>
                            <td>{{ $payment->payer_name }}</td>
                            <td>{{ $payment->payer_email }}</td>
                            <td>{{ $payment->invoice_ref }}</td>
                            <td>{{ $payment->payment_type }}</td>
                            <td>{{ $payment->currency }}</td>
                            <td>{{ $payment->payment_amount }}</td>
                            <td>{{ $payment->payment_status }}</td>
                            <td>
                            <a href="payments/{{$payment->id}}" class="btn btn-primary">Show</a>
                            <a href="payments/{{$payment->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="payments/{{$payment->id}}" method="POST" class="d-inline">
                                {{ csrf_field() }}
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div> 
    </div>
</div>
@endsection
