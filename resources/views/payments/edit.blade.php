@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit Post') }}</div>
                @if ($message = Session::get('success'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>    
                    <strong>{{ $message }}</strong>
                </div>
                @endif
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <form action="/payments/{{$payment->id}}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="">Payment Freelancer Reference</label>
                            <input type="text" name="freelancer_ref" value="{{$payment->freelancer_ref}}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">Payer Name</label>
                            <input name="payer_name" id="" class="form-control" value="{{$payment->payer_name}}"></input>
                        </div>

                        <div class="form-group">
                            <label for="">Payer Email</label>
                            <input type="text" name="payer_email" class="form-control" value="{{$payment->payer_email}}"></input>
                        </div>

                        <div class="form-group">
                            <label for="">Invoice Reference</label>
                            <input type="text" name="invoice_ref" class="form-control" value="{{$payment->invoice_ref}}"></input>
                        </div>

                        <div class="form-group">
                            <label for="">Payment Type</label>
                            <input type="text" name="payment_type" class="form-control" value="{{$payment->payment_type}}"></input>
                        </div>

                        <div class="form-group">
                            <label for="">Currency</label>
                            <input type="text" name="currency" class="form-control" value="{{$payment->currency}}"></input>
                        </div>

                        <div class="form-group">
                            <label for="">Payment Amount</label>
                            <input type="number" name="payment_amount" class="form-control" value="{{$payment->payment_amount}}"></input>
                        </div>

                        <div class="form-group">
                            <label for="">Payment Status</label>
                            <input type="text" name="payment_status" class="form-control" value="{{$payment->payment_status}}"></input>
                        </div>
                        
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection