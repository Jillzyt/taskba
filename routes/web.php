<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/home', [App\Http\Controllers\PaymentController::class, 'index'])->name('home');
Route::get('payments', [App\Http\Controllers\PaymentController::class, 'index'])->name('payments.index');
Route::get('payments/create', [App\Http\Controllers\PaymentController::class, 'create']);
Route::post('payments', [App\Http\Controllers\PaymentController::class, 'store']);
Route::get('payments/{payment}/edit', [App\Http\Controllers\PaymentController::class, 'edit']);
Route::get('payments/{payment}', [App\Http\Controllers\PaymentController::class, 'show']);
Route::post('payments/{payment}', [App\Http\Controllers\PaymentController::class, 'update']);
Route::delete('payments/{payment}', [App\Http\Controllers\PaymentController::class, 'destroy'])->name('payments.delete');